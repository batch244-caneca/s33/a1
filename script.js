//Get all title
fetch("https://jsonplaceholder.typicode.com/todos/")
.then(response => response.json())
.then(json => {console.log(json.map(todo => (todo.title)))});

//specific
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(response => console.log(`The item "${response.title}" on the list has a status of ${response.completed}`));

//using POST
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(response => console.log(response))

fetch("https://jsonplaceholder.typicode.com/todos/", 
	{
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			completed: false,
			title: "Created To Do List Item",
			userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));

//Update using PUT
fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "Updated To Do List Item",
			description: "To update the my to do list with a different data structure",
			status: "Pending",
			dateCompleted: "Pending",
			userId: 1
		})
	}
)
.then(response => response.json())
.then(json => console.log(json));


// using PATCH
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	status: 'Complete',
	  	dateCompleted: '07/09/21'
	})
})
.then(response => response.json())
.then(json => console.log(json));

// DEL
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE',
});

